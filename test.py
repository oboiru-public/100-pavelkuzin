def compress(string):
    if len(string) < 2:
        return string

    ret = string[0]
    count = 1
    for i in string[1:]:
        if i == ret[-1]:
            # совпадает, увеличиваем счетчик и новый цикл
            count += 1
            continue
        if count > 1:
            # не совпадает, добавляем кол-во предыдущих символов
            ret += str(count)
            count = 1
        # новый символ
        ret += i
    return f"{ret}{count}" if count > 1 else ret


result = compress("abbehhhhhtaaa")
print(result)

expected = "ab2eh5ta3"
assert result == expected

assert compress("") == ""
assert compress("a") == "a"
assert compress("aaa") == "a3"
assert compress("aaab") == "a3b"
assert compress("aaabb") == "a3b2"
assert compress("aaabbaaa") == "a3b2a3"
