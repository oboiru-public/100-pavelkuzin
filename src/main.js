/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from "./App.vue";

// Composables
import { createApp } from "vue";

// Plugins
import { registerPlugins } from "@/plugins";
import stringToColor from "string-to-color";

const app = createApp(App);

registerPlugins(app);

app.mount("#app");

// Axios
import { axiosSetUp } from "./@helpers/axios";
axiosSetUp();

// Filters
app.config.globalProperties.$filters = {
  auto_color(value, theme) {
    if (theme === "dark") {
      return stringToColor("black " + value);
    } else if (theme === "light") {
      return stringToColor("white " + value);
    }
    return stringToColor(value);
  },

  money(value) {
    // 123 321 or 1.23 млн.
    if (!value) return 0;
    if (value > 10 ** 6) {
      return (Number(value) / 10 ** 6).toFixed(2) + " млн.";
    }
    return Math.round(value).toLocaleString("ru");
    // return (Number(value) / 10**6).toFixed(2) + " млн."
  },

  phone(value) {
    // from +79109999999
    // to +7 910 999 9999
    if (value.length >= 11) {
      return `${value.slice(-12, -10)} ${value.slice(-10, -7)} ${value.slice(
        -7,
        -4
      )} ${value.slice(-4, -2)} ${value.slice(-2)}`;
    }
    return value;
  },
};
