// Composables
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("@/layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        name: "main",
        component: () => import("@/views/Main.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach(async (to) => {
  // redirect to login page if not logged in and trying to access a restricted page
  // const loginPages = ["/login"];
  // const authRequired = !loginPages.includes(to.path);
  // const authStore = useAuthStore();
  //
  // if (authRequired && !authStore.refreshToken) {
  //   authStore.returnUrl = to.fullPath;
  //   return "/login";
  // }
});
export default router;
