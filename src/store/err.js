// 📍 Обработчик ошибок
// show           - видимость
// add(data)      - добавить новую ошибку
//
import { defineStore } from "pinia";
import { ref } from "vue";

export const useErrStore = defineStore("err", () => {
  // 📍 Data
  const id = ref(0);
  const show = ref(false);
  const template = {
    title: "",
    message: "",
    status: "info",
    from: "",
    sound: false,
  };
  const data = ref({ ...template });
  const sounds = {
    error: "/sounds/error.mp3",
    success: "/sounds/success.mp3",
  };

  // 📍 Functions
  const add = (newData) => {
    show.value = false;
    if (!(typeof newData === "object")) {
      console.log("ошибка должна быть object а не вот это вот", newData);
      return;
    }
    if (newData.sound) {
      const sound = sounds[newData.status];
      if (sound) {
        const audio = new Audio(sound);
        audio.play();
      }
    }
    data.value = { ...template, ...newData };
    show.value = true;
    id.value++;
  };

  return {
    show,
    data,
    add,
    id,
  };
});
