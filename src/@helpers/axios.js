// 📍 Axios intercepter
// Рулит автоматическим обновлением токена, когда тот истечет
//

import axios from "axios";

export function axiosSetUp() {
  axios.defaults.baseURL = "/";
  axios.defaults.method = "post";

  axios.interceptors.request.use(
    // преднастройка axios запросов
    function (config) {
      return config;
    },
    function (error) {
      console.log("axios request error: " + error);
      return Promise.reject(error);
    }
  );

  axios.interceptors.response.use(
    // преднастройка axios ответов
    function (response) {
      return response;
    },
    async function (error) {
      const originalRequest = error.config;
      // if (error.response.status === 401) {
      //   console.log("need refresh");
      //   const isRefresh = await stAuth.makeRefresh();
      //   console.log("refresh", isRefresh);
      //   if (isRefresh) {
      //     return axios(originalRequest);
      //   } else {
      //     stAuth.makeLogout();
      //   }
      // }
      // if (error.response.data) {
      //   stErr.add({
      //     title: "сервер вернул ошибку " + error.response.status,
      //     message: error.response.data.error,
      //     status: "error",
      //     from: error.request.responseURL,
      //   });
      // }
      return Promise.reject(error);
    }
  );
}
