export function replaceUTCtoISOinJsonValues(json) {
  // принимает Object
  // замена values UTC на ISO date для 1го уровня словаря
  for ( var k in json ) {
    if (/^([0-9]{10})$/.test(json[k])) {
      json[k] = new Date(json[k] * 1000)
    }
  }
  return json
}

export function randomColor() {
  return "#" + Math.floor(Math.random()*16777215).toString(16);
}