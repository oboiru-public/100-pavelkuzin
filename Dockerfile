FROM node:lts-alpine3.18 AS builder
WORKDIR /app
COPY package*.json ./
COPY src/ ./src
COPY public ./public
COPY vite.config.js ./
COPY index.html ./
COPY yarn.lock ./

RUN yarn install
RUN yarn build
RUN rm -rf ./src
RUN rm -rf ./node_modules


FROM nginx:alpine AS deploy

# готовлю для base_path = marketplace
WORKDIR /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

COPY --from=builder /app/dist/ ./

ENTRYPOINT ["nginx", "-g", "daemon off;"]
