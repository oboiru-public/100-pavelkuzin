print("not return" and "return")
print("return" or "not return")

# a = [1, 2]
# a.extend("add")
# print(a)
#
# [1, 2, "a", "d", "d"]

# Типы данных
nc = [str, bytes, int, float, complex, bool, None, tuple, frozenset]
cg = [list, set, dict, bytearray, memoryview]


def comprehensions():
    _dict, _list, _set = (
        {i: i**2 for i in range(5)},
        [i for i in range(5)],
        (i for i in range(5)),
    )
    pass


# область видимости
# global a  # глобальная
# nonlocal b  # не локальная


def my_map():
    a = [i for i in range(5)]
    map(lambda x: x**2, a)
    # [0, 1, 4, 9, 16]


def my_filter():
    a = [i for i in range(5)]
    filter(lambda x: x % 2 == 0, a)
    # [0, 2, 4]


def my_zip():
    a = [i for i in range(5)]
    b = [i**2 for i in range(5)]
    c = [i for i in range(8, 10)]
    zip(a, b, c)
    # [(0, 0, 8), (1, 1, 9)]


# логарифмическая сложность Big-O
# list -> O(n)
# set -> O(1)
# dict -> O(1)


## Сложность
## типов
##
## Lambda
## анонимные функции
##
## Замыкание?
##
## Менеджер контекста?
##
## Exceptions
##
## ООП
##
## Типизация ?
# Динамическая
# Статическая

## union?

## magic methods

## кэш профилирование?
# профилировани - замер производительности с целью найти узкие места

## Итератор
## Генератор

## Корутина coroutine

## иммутабельность

## дандерметоды

## контекст менеджер

## меджик или дандер методы
